# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_remove_comment_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='created_on',
        ),
    ]
