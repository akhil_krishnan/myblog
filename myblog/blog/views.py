from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.contrib import auth
from django.http import HttpResponseRedirect, HttpResponse
from blog.models import Post, Comment, UserProfile
from django.template import RequestContext
from blog.forms import RegForm, CommentForm, UserProfileForm
from django.views.decorators.csrf import csrf_exempt
import pdb
from django.contrib.auth.models import User

# Create your views here.


def index(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('index.html', c)


@csrf_exempt
def edit(request):
    context=RequestContext(request)
    user = request.user
    profile = UserProfile.objects.get(user=user)
    if request.method == "POST":
      profile.name = request.POST['name']
      profile.address = request.POST['address']
      profile.phone = request.POST['phone']
      profile.save()
      return render_to_response('ok.html')
    return render_to_response('edit.html' ,{'profile':profile, "user": user},context)

@csrf_exempt
def authcheck(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = auth.authenticate(username=username, password=password)
    if user is not None:
        auth.login(request,user)
        return HttpResponseRedirect('/profile')
    else:
        return HttpResponse('Invalid login details')

@csrf_exempt
def loggedin(request,p_id):
    post = Post.objects.get(pk=p_id)
    if request.method == 'POST':
      form = CommentForm(request.POST)
      if form.is_valid():
        c = form.save(commit=False)
        c.name = request.user
        c.post =  post
        c.save()
    context = RequestContext(request)
    t = CommentForm()
    return render_to_response('loggedin.html',
              {'post': post, 'textform': t 
                 },context
               )
    
@csrf_exempt
def register(request):
    context = RequestContext(request)
    registered = False
    if request.method == 'POST':
        u_form = RegForm(data=request.POST)
        profile_form = UserProfileForm(request.POST)
        if u_form.is_valid() and profile_form.is_valid():
            user = u_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.save()
            registered = True
        if registered:
            return render_to_response('Registersuccess.html')
    u_form = RegForm()
    profile_form = UserProfileForm()
    return render_to_response('register.html',
        {'user_form': u_form, 
        'registered': registered,'profile_form': profile_form},
              context)

@csrf_exempt
def eventlist(request):
    post = Post.objects.all()
    context = RequestContext(request)
    return render_to_response('eventlist.html', {'post':post},context)

def profile(request):
    details = UserProfile.objects.get(user=request.user)
    return render_to_response("profile.html",
        {"full_name": details.name, 'address': details.address,
        'phone': details.phone})

def logout(request):
   auth.logout(request)
   return render_to_response('logout.html')



