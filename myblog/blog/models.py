from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Post(models.Model):
	title = models.CharField(max_length = 100)
	text = models.TextField()

	def __str__(self):
	  return self.title

	


class Comment(models.Model):
	name = models.CharField(max_length=20, null=True)
	text = models.TextField()
	post = models.ForeignKey(Post)
	

	def __str__(self):
	  return self.text


class UserProfile(models.Model):

    user = models.OneToOneField(User)
    name = models.CharField(max_length=20)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=30)

    def __str__(self):
        return '%s,%s,%s,%s' % (self.name, self.address, self.phone)
